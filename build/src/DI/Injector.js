"use strict";
var __spreadArray = (this && this.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Injector = void 0;
exports.Injector = new /** @class */ (function () {
    function class_1() {
    }
    class_1.prototype.resolve = function (target) {
        var tokens = Reflect.getMetadata('design:paramtypes', target) || [], injections = tokens.map(function (token) { return exports.Injector.resolve(token); });
        return new (target.bind.apply(target, __spreadArray([void 0], injections)))();
    };
    return class_1;
}());
//# sourceMappingURL=Injector.js.map