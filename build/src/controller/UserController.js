"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var typeorm_1 = require("typeorm");
var controller_decorator_1 = require("../decorator/controller-decorator");
var Get_1 = require("../decorator/route-decorator/Get");
var Post_1 = require("../decorator/route-decorator/Post");
var User_1 = require("../entity/User");
var UserRepo_1 = require("../repository/UserRepo");
var authentication_1 = require("../utils/authentication");
var UserController = /** @class */ (function () {
    function UserController() {
        this.userRepo = typeorm_1.getCustomRepository(UserRepo_1.UserRepository);
    }
    UserController.prototype.index = function (req, res, next) {
        return res.send('User overview');
    };
    UserController.prototype.details = function (req, res, next) {
        return __awaiter(this, void 0, void 0, function () {
            var username, user;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('USERNAME');
                        username = req.params.username;
                        return [4 /*yield*/, this.userRepo.findOne({ username: username })];
                    case 1:
                        user = _a.sent();
                        return [2 /*return*/, res.send({ email: user.email, username: user.username })];
                }
            });
        });
    };
    UserController.prototype.create = function (req, res, next) {
        return __awaiter(this, void 0, void 0, function () {
            var userBody, user, userCreated, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('test');
                        console.log(req.body.user);
                        userBody = req.body.user;
                        user = new User_1.User();
                        console.log(user);
                        user.email = userBody.email;
                        console.log(user);
                        user.username = userBody.username;
                        console.log(user);
                        try {
                            user.setPassword(userBody.password);
                        }
                        catch (e) {
                            throw (e);
                        }
                        console.log(user);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.userRepo.save(user)];
                    case 2:
                        userCreated = _a.sent();
                        return [2 /*return*/, res.json(userCreated.toAuthJson())];
                    case 3:
                        err_1 = _a.sent();
                        console.log('IL Y A UNE ERREUR');
                        next(err_1);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    UserController.prototype.allo = function (req, res, next) {
        return res.json({ allo: "allo" });
    };
    __decorate([
        Get_1.Get('/'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Object, Function]),
        __metadata("design:returntype", void 0)
    ], UserController.prototype, "index", null);
    __decorate([
        Get_1.Get('/:username', authentication_1.default.required),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Object, Function]),
        __metadata("design:returntype", Promise)
    ], UserController.prototype, "details", null);
    __decorate([
        Post_1.Post('/create'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Object, Function]),
        __metadata("design:returntype", Promise)
    ], UserController.prototype, "create", null);
    __decorate([
        Get_1.Get('/allo', authentication_1.default.optional),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Object, Function]),
        __metadata("design:returntype", void 0)
    ], UserController.prototype, "allo", null);
    UserController = __decorate([
        controller_decorator_1.Controller('/user'),
        __metadata("design:paramtypes", [])
    ], UserController);
    return UserController;
}());
exports.default = UserController;
//# sourceMappingURL=UserController.js.map