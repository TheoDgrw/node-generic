"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Pizza = void 0;
var typeorm_1 = require("typeorm");
var Pizza = /** @class */ (function () {
    function Pizza() {
    }
    Object.defineProperty(Pizza.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (name) {
            this._name = name;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Pizza.prototype, "price", {
        get: function () {
            return this._price;
        },
        set: function (price) {
            this._price = price;
        },
        enumerable: false,
        configurable: true
    });
    __decorate([
        typeorm_1.PrimaryGeneratedColumn(),
        __metadata("design:type", Number)
    ], Pizza.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], Pizza.prototype, "name", null);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Number),
        __metadata("design:paramtypes", [Number])
    ], Pizza.prototype, "price", null);
    Pizza = __decorate([
        typeorm_1.Entity()
    ], Pizza);
    return Pizza;
}());
exports.Pizza = Pizza;
//# sourceMappingURL=Pizza.js.map