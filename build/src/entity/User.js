"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
var typeorm_1 = require("typeorm");
var crypto_1 = require("crypto");
var jsonwebtoken_1 = require("jsonwebtoken");
var User = /** @class */ (function () {
    function User() {
    }
    User.prototype.setPassword = function (password) {
        console.log(password);
        this.salt = crypto_1.randomBytes(16).toString('hex');
        console.log(this.salt);
        this.hash = crypto_1.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
        console.log(this.hash);
    };
    User.prototype.validPassword = function (password) {
        var hash = crypto_1.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
        return this._hash === hash;
    };
    User.prototype.generateJwt = function () {
        var today = new Date();
        var exp = new Date(today);
        exp.setDate(today.getDate() + 60);
        return jsonwebtoken_1.sign({
            id: this.id,
            username: this._username,
            exp: exp.getTime() / 1000
        }, process.env.SECRET);
    };
    User.prototype.toAuthJson = function () {
        return {
            username: this._username,
            email: this._email,
            token: this.generateJwt()
        };
    };
    Object.defineProperty(User.prototype, "username", {
        get: function () {
            return this._username;
        },
        set: function (username) {
            this._username = username;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(User.prototype, "email", {
        get: function () {
            return this._email;
        },
        set: function (email) {
            this._email = email;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(User.prototype, "hash", {
        get: function () {
            return this._hash;
        },
        set: function (hash) {
            this._hash = hash;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(User.prototype, "salt", {
        get: function () {
            return this._salt;
        },
        set: function (salt) {
            this._salt = salt;
        },
        enumerable: false,
        configurable: true
    });
    __decorate([
        typeorm_1.PrimaryGeneratedColumn(),
        __metadata("design:type", Number)
    ], User.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column({ nullable: false, unique: true }),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], User.prototype, "username", null);
    __decorate([
        typeorm_1.Column({ nullable: false, unique: true }),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], User.prototype, "email", null);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], User.prototype, "hash", null);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], User.prototype, "salt", null);
    User = __decorate([
        typeorm_1.Entity()
    ], User);
    return User;
}());
exports.User = User;
//# sourceMappingURL=User.js.map