"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Delete = void 0;
var Delete = function (path) {
    var middlewares = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        middlewares[_i - 1] = arguments[_i];
    }
    return function (target, propertyKey) {
        if (!Reflect.hasMetadata('routes', target.constructor)) {
            Reflect.defineMetadata('routes', [], target.constructor);
        }
        var routes = Reflect.getMetadata('routes', target.constructor);
        routes.push(__assign(__assign({ requestMethod: 'delete', path: path }, middlewares), { methodName: propertyKey }));
        Reflect.defineMetadata('routes', routes, target.constructor);
    };
};
exports.Delete = Delete;
//# sourceMappingURL=Delete.js.map