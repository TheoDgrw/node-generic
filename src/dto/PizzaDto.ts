

export interface PizzaDto {
    name: string,
    price: number
}