import * as passport from "passport";
import { Strategy } from "passport-local";
import { getCustomRepository } from "typeorm";
import { UserRepository } from "../repository/UserRepo";

function initPassport() {
    const userRepo = getCustomRepository(UserRepository);
    
    passport.use(new Strategy({
        usernameField: 'User[email]',
        passwordField: 'User[password]'
    }, async (email, password, done) => {
        const user = await userRepo.findOne({ email: email });
        if (!user) {
            return done(null, false, { message: 'invalid email' });
        } else if (!user.validPassword(password)) {
            return done(null, false, { message: 'invalid password' });
        }
    
        return done(null, user);
    }));
}

export default initPassport;