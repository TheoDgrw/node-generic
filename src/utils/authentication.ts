import { Request } from "express";
import * as jwt from "express-jwt";

function getTokenFromHeader(req: Request) {
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Token') {
        return req.headers.authorization.split(' ')[1];
    }

    return null;
}

const auth = {
    required: jwt({
        secret: process.env.SECRET,
        userProperty: 'payload',
        getToken: getTokenFromHeader,
        algorithms: ['sha512'],
        credentialsRequired: true
    }),
    optional: jwt({
        secret: process.env.SECRET,
        userProperty: 'payload',
        getToken: getTokenFromHeader,
        algorithms: ['sha512'],
        credentialsRequired: false
    })
}

export default auth;