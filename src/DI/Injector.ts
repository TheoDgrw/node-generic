import { getCustomRepository, Repository } from "typeorm";
import { UserRepository } from "../repository/UserRepo";
import Type from "./interface/Type";



export const Injector = new class {
    resolve<T>(target: Type<any>): T {
        let tokens = Reflect.getMetadata('design:paramtypes', target) || [],
            injections = tokens.map(token => Injector.resolve<any>(token));

        return new target(...injections);
    }
}