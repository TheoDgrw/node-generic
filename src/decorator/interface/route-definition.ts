import { NextFunction, Request, Response, RequestHandler } from "express";
import { RequestHandler as JwtRequestHandler } from "express-jwt";


export default interface RouteDefinition {
    requestMethod: 'get' | 'post' | 'delete' | 'put' | 'options';
    path: string;
    middlewares?: RequestHandler[],
    methodName: string
}