import { NextFunction, Request, Response, RequestHandler } from "express";
// import { RequestHandler } from "express-jwt";
import RouteDefinition from "../interface/route-definition";


export const Get = (
    path: string,
    ...middlewares: RequestHandler[]
): MethodDecorator => {
    return (target, propertyKey: string): void => {
        if (!Reflect.hasMetadata('routes', target.constructor)) {
            Reflect.defineMetadata('routes', [], target.constructor);
        }

        const routes = Reflect.getMetadata('routes', target.constructor) as Array<RouteDefinition>;

        routes.push({
            requestMethod: 'get',
            path,
            middlewares: [...middlewares],
            methodName: propertyKey
        });
        Reflect.defineMetadata('routes', routes, target.constructor);
    }
}