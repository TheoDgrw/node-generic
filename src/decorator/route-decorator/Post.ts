import { NextFunction, Request, Response } from "express";
import { RequestHandler } from "express-jwt";
import RouteDefinition from "../interface/route-definition";


export const Post = (
    path: string,
    ...middlewares: Array<Parameters<(req: Request, res: Response, next?: NextFunction) => any> | RequestHandler>
): MethodDecorator => {
    return (target, propertyKey: string): void => {
        if (!Reflect.hasMetadata('routes', target.constructor)) {
            Reflect.defineMetadata('routes', [], target.constructor);
        }

        const routes = Reflect.getMetadata('routes', target.constructor) as Array<RouteDefinition>;

        routes.push({
            requestMethod: 'post',
            path,
            ...middlewares,
            methodName: propertyKey
        });
        Reflect.defineMetadata('routes', routes, target.constructor);
    }
}