import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { randomBytes, pbkdf2Sync } from "crypto";
import { sign } from "jsonwebtoken";

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    public readonly id: number;

    private _username: string;
    private _email: string;
    private _hash: string;
    private _salt: string;

    public setPassword(password) {
        console.log(password);
        this.salt = randomBytes(16).toString('hex');
        console.log(this.salt);
        this.hash = pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
        console.log(this.hash);
    }

    public validPassword(password): boolean {
        const hash = pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
        return this._hash === hash;
    }

    private generateJwt(): string {
        const today = new Date();
        const exp = new Date(today);
        exp.setDate(today.getDate() + 60);

        return sign({
            id: this.id,
            username: this._username,
            exp: exp.getTime() / 1000
        }, process.env.SECRET);
    }

    public toAuthJson() {
        return {
            username: this._username,
            email: this._email,
            token: this.generateJwt()
        }
    }

    @Column({ nullable: false, unique: true })
    public get username(): string {
        return this._username;
    }
    public set username(username: string) {
        this._username = username;
    }

    @Column({ nullable: false, unique: true })
    public get email(): string {
        return this._email;
    }
    public set email(email: string) {
        this._email = email;
    }

    @Column()
    public get hash(): string {
        return this._hash;
    }
    public set hash(hash: string) {
        this._hash = hash;
    }

    @Column()
    public get salt(): string {
        return this._salt;
    }
    public set salt(salt: string) {
        this._salt = salt;
    }
}