import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class Pizza {

    @PrimaryGeneratedColumn()
    public readonly id: number;

    private _name: string;
    private _price: number;

    @Column()
    public get name(): string {
        return this._name;
    }
    public set name(name: string) {
        this._name = name;
    }

    @Column()
    public get price(): number {
        return this._price;
    }
    public set price(price: number) {
        this._price = price;
    }

}
