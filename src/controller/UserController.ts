import { NextFunction, Request, Response } from "express";
import { getCustomRepository } from "typeorm";
import { Controller } from "../decorator/controller-decorator";
import { Get } from "../decorator/route-decorator/Get";
import { Post } from "../decorator/route-decorator/Post";
import { UserDto } from "../dto/UserDto";
import { User } from "../entity/User";
import { UserRepository } from "../repository/UserRepo";
import auth from "../utils/authentication";


@Controller('/user')
export default class UserController {
    private userRepo: UserRepository;

    constructor() {
        this.userRepo = getCustomRepository(UserRepository);
    }

    @Get('/')
    index(req: Request, res: Response, next: NextFunction) {
        return res.send('User overview');
    }

    @Get('/:username', auth.required)
    async details(req: Request, res: Response, next: NextFunction) {
        console.log('USERNAME');
        const username = req.params.username;
        const user = await this.userRepo.findOne({username: username});

        return res.send({email: user.email, username: user.username});
    }

    @Post('/create')
    async create(req: Request, res: Response, next: NextFunction) {
        console.log('test');
        console.log(req.body.user);
        const userBody: UserDto = req.body.user;
        const user = new User();
        console.log(user);

        user.email = userBody.email;
        console.log(user);
        user.username = userBody.username;
        console.log(user);
        try {
            user.setPassword(userBody.password);
        } catch (e) {
            throw(e);
        }

        console.log(user);
        try {
            const userCreated = await this.userRepo.save(user);

            return res.json(userCreated.toAuthJson());
        } catch (err) {
            console.log('IL Y A UNE ERREUR');
            next(err);
        }
    }

    @Get('/allo', auth.optional)
    allo(req: Request, res: Response, next: NextFunction) {
        
        return res.json({ allo: "allo" });
    }
}