import { NextFunction, Request, Response } from "express";
import { getCustomRepository } from "typeorm";
import { Controller } from "../decorator/controller-decorator";
import { Get } from "../decorator/route-decorator/Get";
import { Post } from "../decorator/route-decorator/Post";
import Service from "../DI/Service";
import { PizzaDto } from "../dto/PizzaDto";
import { Pizza } from "../entity/Pizza";
import { PizzaRepository } from "../repository/PizzaRepo";


@Controller('/pizza')
export default class PizzaController {
    private pizzaRepo: PizzaRepository;

    constructor() {
        this.pizzaRepo = getCustomRepository(PizzaRepository);
    }

    @Get('/')
    async getAllPizzas(req: Request, res: Response, next: NextFunction) {
        const pizzas = await this.pizzaRepo.find();

        return res.json(pizzas);
    }

    @Post('/')
    async addPizza(req: Request, res: Response, next: NextFunction) {
        const pizza: PizzaDto = req.body.pizza;
        let newPizza = new Pizza();
        newPizza.name = pizza.name;
        newPizza.price = pizza.price;
        const ormRes = await this.pizzaRepo.save(newPizza);

        return res.json(ormRes);
    }
}