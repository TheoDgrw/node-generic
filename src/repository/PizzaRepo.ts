import { EntityRepository, Repository } from "typeorm";
import { Pizza } from "../entity/Pizza";



@EntityRepository(Pizza)
export class PizzaRepository extends Repository<Pizza> {
    
}