import "reflect-metadata";
import * as express from "express";
import { urlencoded, json } from "body-parser";
import initPassport from "../src/utils/passport";
import UserController from "../src/controller/UserController";
import PizzaController from "../src/controller/PizzaController";
import RouteDefinition from "../src/decorator/interface/route-definition";
import { Injector } from "../src/DI/Injector";
import { Connection, createConnection } from "typeorm";

const app = express();
app.use(urlencoded({ extended: false }));
app.use(json());

export const connection: Promise<Connection> = createConnection();

app.listen(process.env.PORT, async () => {
    connection.then(() => {
        initPassport();
        [
            UserController,
            PizzaController
        ].forEach(controller => {
            const instance = Injector.resolve(controller);
            const prefix = Reflect.getMetadata('prefix', controller);
            const routes: Array<RouteDefinition> = Reflect.getMetadata('routes', controller);
            
            routes.forEach(route => {
                console.log(route.middlewares);
                if (route.middlewares !== undefined && route.middlewares.length !== 0) {
                    app[route.requestMethod](prefix + route.path, ...route.middlewares, (req: express.Request, res: express.Response, next: express.NextFunction) => {
                        instance[route.methodName](req, res, next);
                    });
                } else  {
                    app[route.requestMethod](prefix + route.path, (req: express.Request, res: express.Response, next: express.NextFunction) => {
                        instance[route.methodName](req, res, next);
                    });
                }
            });
        });
    }).catch(e => {
        throw(e);
    });

    console.log('listening on port ' + process.env.PORT);
});
