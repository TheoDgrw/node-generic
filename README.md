# Da Pizza mucho italian the retaurante en ligne

Test app:

## Pizza:

1. To create a pizza, send a POST request to this end: https://simplon-pizza.herokuapp.com/pizza
with a json body like this one:
```
{
    "pizza": {
        "name": "catalane",
        "price": "20"
    }
}
```
2. To find all pizzas send a GET request to this end: https://simplon-pizza.herokuapp.com/pizza

## User:

1. To create a user, send a POST request to this end: https://simplon-pizza.herokuapp.com/user/create
with a json body like this one:
```
{
    "user": {
        "username": "eltheo",
        "email": "test1@gmail.com",
        "password": "password"
    }
}
```
2. In the response to the above request you have a jwt token which you can store to send next requests
3. Get details from user with a GET request to this end: https://simplon-pizza.herokuapp.com/user/<username>
you need to add the jwt to the header "Authorization" with the value : "Token <myjwt>"

## Steps to run this project locally:

1. Run `yarn i` command
2. Setup database settings inside `.env` file with your postgres credentials in `TYPEORM_USERNAME` `TYPEORM_PASSWORD` `TYPEORM_DATABASE` `TYPEORM_PORT`
3. Run `docker-compose up`, if docker isn't installed, install docker
4. create the pizza database
5. Run `yarn run migration:run` command
4. Run `yarn run dev` command
